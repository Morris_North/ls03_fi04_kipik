/**
 * Created / Modified by 
 + @author Erki_
 * DO NOT COPY PASTE
 */
package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * @author Erkan Kipik
 * @version 0.1
 */
public class FormAendern extends JFrame {

	private JPanel pnlMain;
	private JTextField txtTextChange;
//	private JButton btnTextBlue;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormAendern frame = new FormAendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormAendern() {
		setTitle("Form_aendern");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 600);
		pnlMain = new JPanel();
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlMain);
		pnlMain.setLayout(null);
		
		JLabel lblChangeText = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lblChangeText.setHorizontalAlignment(SwingConstants.CENTER);
		lblChangeText.setBounds(10, 11, 364, 64);
		pnlMain.add(lblChangeText);
		
		JLabel lblBgColor = new JLabel("Aufgabe 1: Hingtergrundfarbe \u00E4ndern");
		lblBgColor.setBounds(10, 86, 364, 26);
		pnlMain.add(lblBgColor);
		
		JButton btnBgRed = new JButton("Rot");
		btnBgRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				pnlMain.setBackground(Color.RED);
			}
		});
		btnBgRed.setBounds(10, 113, 116, 26);
		pnlMain.add(btnBgRed);
		
		JButton btnBgBlue = new JButton("Blau");
		btnBgBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.BLUE);
			}
		});
		btnBgBlue.setBounds(258, 113, 116, 26);
		pnlMain.add(btnBgBlue);
		
		JButton btnBgGr�n = new JButton("Gr\u00FCn");
		btnBgGr�n.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.GREEN);
			}
		});
		btnBgGr�n.setBounds(134, 113, 116, 26);
		pnlMain.add(btnBgGr�n);
		
		JButton btnBgGelb = new JButton("Gelb");
		btnBgGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(Color.YELLOW);
			}
		});
		btnBgGelb.setBounds(10, 147, 116, 26);
		pnlMain.add(btnBgGelb);
		
		JButton btnBgDefault = new JButton("Standardfarbe");
		btnBgDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(UIManager.getColor("menu"));
			}
		});
		btnBgDefault.setBounds(134, 147, 116, 26);
		pnlMain.add(btnBgDefault);
		
		JButton btnBgCustom = new JButton("Farbe w\u00E4hlen");
		btnBgCustom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pnlMain.setBackground(JColorChooser.showDialog(lblChangeText, "Neue Farbe w�hlen", Color.white));
			}
		});
		btnBgCustom.setBounds(258, 147, 116, 26);
		pnlMain.add(btnBgCustom);
		
		JLabel lblFormatText = new JLabel("Aufgabe 2: Text formatieren");
		lblFormatText.setBounds(10, 179, 364, 26);
		pnlMain.add(lblFormatText);
		
		JButton btnTextArial = new JButton("Arial");
		btnTextArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setFont(new Font("Arial", Font.PLAIN, lblChangeText.getFont().getSize()));
			}
		});
		btnTextArial.setBounds(10, 207, 116, 26);
		pnlMain.add(btnTextArial);
		
		JButton btnTextCourier = new JButton("Courier New");
		btnTextCourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setFont(new Font("Courier New", Font.PLAIN, lblChangeText.getFont().getSize()));
			}
		});
		btnTextCourier.setBounds(258, 207, 116, 26);
		pnlMain.add(btnTextCourier);
		
		JButton btnTextComic = new JButton("Comic Sans MS");
		btnTextComic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setFont(new Font("Comic Sans MS", Font.PLAIN, lblChangeText.getFont().getSize()));
			}
		});
		btnTextComic.setBounds(134, 207, 116, 26);
		pnlMain.add(btnTextComic);
		
		txtTextChange = new JTextField();
		txtTextChange.setText("Hier bitte Text eingeben");
		txtTextChange.setBounds(10, 237, 364, 24);
		pnlMain.add(txtTextChange);
		txtTextChange.setColumns(10);
		
		JButton btntextWrite = new JButton("Ins Label schreiben");
		btntextWrite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setText(txtTextChange.getText());
			}
		});
		btntextWrite.setBounds(10, 266, 180, 26);
		pnlMain.add(btntextWrite);
		
		JButton btnTextDelete = new JButton("Text im Label l\u00F6schen");
		btnTextDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setText("");
			}
		});
		btnTextDelete.setBounds(194, 266, 180, 26);
		pnlMain.add(btnTextDelete);
		
		JLabel lblTextColor = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblTextColor.setBounds(10, 296, 364, 26);
		pnlMain.add(lblTextColor);
		
		JButton btnTextRed = new JButton("Rot");
		btnTextRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setForeground(Color.RED);
			}
		});
		btnTextRed.setBounds(10, 324, 116, 26);
		pnlMain.add(btnTextRed);
		
		JButton btnTextBlack = new JButton("Schwarz");
		btnTextBlack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setForeground(Color.BLACK);
			}
		});
		btnTextBlack.setBounds(258, 324, 116, 26);
		pnlMain.add(btnTextBlack);
		
		JButton btnTextBlue = new JButton("Blau");
		btnTextBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setForeground(Color.BLUE);
			}
		});
		btnTextBlue.setBounds(134, 324, 116, 26);
		pnlMain.add(btnTextBlue);
		
		JLabel lblTextSize = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lblTextSize.setBounds(10, 354, 364, 26);
		pnlMain.add(lblTextSize);
		
		JButton btnTextSizePlus = new JButton("+");
		btnTextSizePlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setFont(new Font(lblChangeText.getFont().getFamily(), lblChangeText.getFont().getStyle(), lblChangeText.getFont().getSize()+1));
			}
		});
		btnTextSizePlus.setBounds(10, 381, 180, 26);
		pnlMain.add(btnTextSizePlus);
		
		JButton btnTextSizeMinus = new JButton("-");
		btnTextSizeMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setFont(new Font(lblChangeText.getFont().getFamily(), lblChangeText.getFont().getStyle(), (lblChangeText.getFont().getSize() > 1) ? lblChangeText.getFont().getSize()-1 : 1));
			}
		});
		btnTextSizeMinus.setBounds(194, 381, 180, 26);
		pnlMain.add(btnTextSizeMinus);
		
		JLabel lblTextAlign = new JLabel("Aufgabe 5: Textausrichtung");
		lblTextAlign.setBounds(10, 412, 364, 26);
		pnlMain.add(lblTextAlign);
		
		JButton btnTextAlignLeft = new JButton("linksb\u00FCndig");
		btnTextAlignLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnTextAlignLeft.setBounds(10, 440, 116, 26);
		pnlMain.add(btnTextAlignLeft);
		
		JButton btnTextAlignRight = new JButton("rechtsb\u00FCndig");
		btnTextAlignRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnTextAlignRight.setBounds(258, 440, 116, 26);
		pnlMain.add(btnTextAlignRight);
		
		JButton btnTextAlignCenter = new JButton("zentriert");
		btnTextAlignCenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblChangeText.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnTextAlignCenter.setBounds(134, 440, 116, 26);
		pnlMain.add(btnTextAlignCenter);
		
		JLabel lblExit = new JLabel("Aufgabe 6: Programm beenden");
		lblExit.setBounds(10, 469, 364, 26);
		pnlMain.add(lblExit);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnExit.setBounds(10, 498, 364, 52);
		pnlMain.add(btnExit);
	}
}
