package view.menue;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import controller.AlienDefenceController;
import model.User;

import java.awt.CardLayout;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

//TODO create a usermanagement
@SuppressWarnings("serial")
public class CreateUserWindow extends JFrame {
	
	private AlienDefenceController alienDefenceController;
	
	private JPanel contentPane;
	private JTextField tfdName;
	private JLabel lblSurName;
	private JTextField tfdSurName;
	private JTextField tfdBirthday;
	private JLabel lblBirthday;
	private JLabel lblStreet;
	private JTextField tfdStreet;
	private JLabel lblStreetNumber;
	private JTextField tfdStreetNumber;
	private JLabel lblPostalCode;
	private JTextField tfdPostalCode;
	private JLabel lblCity;
	private JTextField tfdCity;
	private JLabel lblLoginName;
	private JTextField tfdLoginName;
	private JLabel lblPassword;
	private JTextField tfdPassword;
	private JLabel lblSalary;
	private JTextField tfdSalary;
	private JLabel lblMaritalStatus;
	private JTextField tfdMaritalStatus;
	private JLabel lblFinalGrade;
	private JTextField tfdFinalGrade;
	private JButton btnCreateUser;
	private JButton btnEdit;
	private JButton btnAbort;
	
	public CreateUserWindow(AlienDefenceController alienDefenceController) {
		this.alienDefenceController = alienDefenceController;
		
		setTitle("Neuen Benutzer Anlegen");
		
		// Allgemeine JFrame-Einstellungen
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 487, 506);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 2, 5, 10));
		contentPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		setVisible(true);
		
		JLabel lblName = new JLabel("Name");
		contentPane.add(lblName);
		
		tfdName = new JTextField();
		contentPane.add(tfdName);
		tfdName.setColumns(10);
		
		lblSurName = new JLabel("Nachname");
		contentPane.add(lblSurName);
		
		tfdSurName = new JTextField();
		contentPane.add(tfdSurName);
		tfdSurName.setColumns(10);
		
		lblBirthday = new JLabel("Geburtstag");
		contentPane.add(lblBirthday);
		
		tfdBirthday = new JTextField();
		contentPane.add(tfdBirthday);
		tfdBirthday.setColumns(10);
		
		lblStreet = new JLabel("Stra\u00DFe");
		contentPane.add(lblStreet);
		
		tfdStreet = new JTextField();
		contentPane.add(tfdStreet);
		tfdStreet.setColumns(10);
		
		lblStreetNumber = new JLabel("Hausnummer");
		contentPane.add(lblStreetNumber);
		
		tfdStreetNumber = new JTextField();
		contentPane.add(tfdStreetNumber);
		tfdStreetNumber.setColumns(10);
		
		lblPostalCode = new JLabel("Postleitzahl");
		contentPane.add(lblPostalCode);
		
		tfdPostalCode = new JTextField();
		contentPane.add(tfdPostalCode);
		tfdPostalCode.setColumns(10);
		
		lblCity = new JLabel("Stadt");
		contentPane.add(lblCity);
		
		tfdCity = new JTextField();
		contentPane.add(tfdCity);
		tfdCity.setColumns(10);
		
		lblLoginName = new JLabel("Login Name");
		contentPane.add(lblLoginName);
		
		tfdLoginName = new JTextField();
		contentPane.add(tfdLoginName);
		tfdLoginName.setColumns(10);
		
		lblPassword = new JLabel("Passwort");
		contentPane.add(lblPassword);
		
		tfdPassword = new JTextField();
		contentPane.add(tfdPassword);
		tfdPassword.setColumns(10);
		
		lblSalary = new JLabel("Gehaltsvorstellung");
		contentPane.add(lblSalary);
		
		tfdSalary = new JTextField();
		contentPane.add(tfdSalary);
		tfdSalary.setColumns(10);
		
		lblMaritalStatus = new JLabel("Familienstand");
		contentPane.add(lblMaritalStatus);
		
		tfdMaritalStatus = new JTextField();
		contentPane.add(tfdMaritalStatus);
		tfdMaritalStatus.setColumns(10);
		
		lblFinalGrade = new JLabel("Endnote");
		contentPane.add(lblFinalGrade);
		
		tfdFinalGrade = new JTextField();
		contentPane.add(tfdFinalGrade);
		tfdFinalGrade.setColumns(10);
		
		btnCreateUser = new JButton("Benutzer Anlegen");
		btnCreateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				BtnCreateUserClicked();
			}
		});
		contentPane.add(btnCreateUser);
		
		btnEdit = new JButton("Editieren");
		btnEdit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnEditClicked();
			}
		});
		contentPane.add(btnEdit);
		
		btnAbort = new JButton("Abbrechen");
		btnAbort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAbortClicked();
			}
		});
		contentPane.add(btnAbort);
		
	}
	
	
	private void BtnCreateUserClicked() {
		boolean empty = false;
		for (Component c : this.contentPane.getComponents()) {
			if (c instanceof JTextField) {
				if(((JTextField) c).getText().isEmpty()) {
					c.setBackground(Color.RED);
					empty = true;
                }
			}
        }
		if(!empty) {
			User user = this.readDataToUser();
			this.alienDefenceController.getUserController().createUser(user);
			this.dispose();
		}
	}
	
	private void btnEditClicked() {
		for (Component c : this.contentPane.getComponents()) {
			if (c instanceof JTextField) {
				if(((JTextField) c).getText().isEmpty()) {
					c.setBackground(UIManager.getColor("Panel.background"));
                }
			}
        }
	}
	
	private void btnAbortClicked() {
		this.dispose();
	}
	
	private User readDataToUser() {
		return new User(0, 
				tfdName.getText(), 
				tfdSurName.getText(), 
				this.parseDate(tfdBirthday.getText()), 
				tfdStreet.getText(),
				tfdStreetNumber.getText(),
				tfdPostalCode.getText(),
				tfdCity.getText(),
				tfdLoginName.getText(),
				tfdPassword.getText(),
				Integer.parseInt(tfdSalary.getText()),
				tfdMaritalStatus.getText(),
				Double.parseDouble(tfdFinalGrade.getText())
				);
	}
	
	private LocalDate parseDate(String date) { //Copy from https://mkyong.com/java8/java-8-how-to-convert-string-to-localdate/
        String pattern = "dd.MM.yyyy";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return LocalDate.parse(date, formatter);
	}
}
