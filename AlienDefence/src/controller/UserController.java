package controller;

import java.sql.SQLException;

import model.User;
import model.persistance.IPersistance;
import model.persistance.IUserPersistance;

/**
 * controller for users
 * @author Clara Zufall
 * Edit by @author Erkan Kipik FI-A 04
 * 
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IPersistance userPersistance) {
		this.userPersistance = userPersistance.getUserPersistance();
	}
	
	/**
	 * Checks if the user to be created exists. If not creates the user.
	 * 
	 * @param user User to be created
	 */
	public void createUser(User user) {
		if(!this.checkUserExistence(user)) {
			this.userPersistance.createUser(user);
		}
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zur�ck
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */
	public User readUser(String username, String passwort) {
		return this.userPersistance.readUser(username);
	}
	
	/**
	 * Updates the given user after checking if he exists or not.
	 * 
	 */
	public void changeUser(User user) {
		if(this.checkUserExistence(user)) {
			this.userPersistance.updateUser(user);
		}
	}
	
	/**
	 * Checks if the user to be deleted exists. If not deletes the user.
	 * 
	 * @param user User to be created
	 */
	public void deleteUser(User user) {
		if(this.checkUserExistence(user)) {
			this.userPersistance.deleteUser(user);
		}
	}
	
	/**
	 * Checks if the password of the given username is correct.
	 * 
	 * @param username Name of User to be checked.
	 * @param passwort Password to be checked.
	 * @return boolean if matches or not.
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = this.readUser(username, passwort);
		return user.getPassword().equals(passwort);
	}
	
	/**
	 * Helper Method
	 */
	private boolean checkUserExistence(User user) {
		return this.userPersistance.readUser(user.getLoginname()) != null;
	}
}
